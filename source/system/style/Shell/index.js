import React from 'react';
import PropTypes from 'prop-types';

import '../reset.scss';
import '../fonts/Urbanist/style.scss';
import './style.scss';

const Shell = ({ children, useRoot }) => {

  if (useRoot) {
    document.getElementById('root').className = "shell";

    return (
      <>{children}</>
    );
  }
  return (
    <div className="shell">
      {children}
    </div>);
};

Shell.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]).isRequired,
  useRoot: PropTypes.bool
};

Shell.defaultProps = {
  useRoot: true
};

export default Shell;
