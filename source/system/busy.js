import { store } from 'store';
import { addBusy, removeBusy } from 'store/slices/system';


export function start (id) {
  store.dispatch(addBusy(id));
}

export function finish (id) {
  store.dispatch(removeBusy(id));
}

export default function busyWhile (id, action) {
  start(id);
  action(()=>finish(id));
}
