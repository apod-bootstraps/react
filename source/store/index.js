import { configureStore } from '@reduxjs/toolkit';

// slices
import system from './slices/system';

export const store = configureStore({
  reducer: {
    system
  },
});
