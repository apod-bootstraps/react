import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  busyResources: []
};

export const systemSlice = createSlice({
  name: 'system',
  initialState,
  reducers: {
    addBusy: (state, payload) => {
      let pos = state.busyResources.findIndex(element => element === payload.resource);

      if (pos !== -1) {
        throw new Error ('Resource identification already in use');
      }

      state.busyResources.push(payload.resource);
    },
    removeBusy: (state, payload) => {
      let pos = state.busyResources.findIndex(element => element === payload.resource);

      if (pos === -1) {
        throw new Error ('Invalid resource identification');
      }

      state.busyResources = state.busyResources.filter(element => element != payload.resource);
    }
  }
});

export const { addBusy, removeBusy } = systemSlice.actions;
export default systemSlice.reducer;
