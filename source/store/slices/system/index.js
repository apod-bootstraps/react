import { combineReducers } from '@reduxjs/toolkit';
import busy, { addBusy, removeBusy } from './busy.js';

export { busy, addBusy, removeBusy };
export default combineReducers({
  busy
});
