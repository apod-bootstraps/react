import React from 'react';
import PropTypes from 'prop-types';

import Media from 'components/Media';

import './style.scss';

const ScreenContent = ({ className }) => {
  return (
    <div className={`screen-content ${className}`}>

      <div className="screen-content_media-container">
        <Media url="https://apod.nasa.gov/apod/image/2403/IM_Odysseus_landing-1100x600.png" />

        <div className="screen-content_credit">
          <span className="screen-content_credit-title">Image Credit:</span> Intuitive Machines
        </div>
      </div>

      <div className="screen-content_title">
        Odysseus on the Moon
      </div>

      <div className="screen-content_explanation">
        Methalox rocket engine firing, Odysseus landing legs absorb first contact with the lunar surface in this wide-angle snapshot from a camera on board the robotic Intuitive Machines Nova-C moon lander. Following the landing on February 22, broken landing legs, visible in the image, ultimately left the lander at rest but tilted. Odysseus gentle lean into a sloping lunar surface preserved the phone booth-sized landers ability to operate, collect solar power, and return images and data to Earth. Its exact landing site in the Moons far south polar region was imaged by NASAs Lunar Reconnaissance Orbiter. Donated by NASA, the American flag seen on the lander&apos;s central panel is 1970 Apollo program flight hardware.
      </div>



    </div >
  );
};

ScreenContent.propTypes = {
  title: PropTypes.string.isRequired,
  className: PropTypes.string
};

ScreenContent.defaultProps = {
  className: '',
};

export default ScreenContent;
