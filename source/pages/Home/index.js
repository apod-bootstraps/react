import React from 'react';

import Header from 'components/Header';
import Content from 'screens/Content';

import './style.scss';

const Home = () => {
  return (
    <div className="page-home">
      <Header title="Atronomy Picture of the Day" />
      <Content className="page-home_content" />
    </div >
  );
};

export default Home;
