import React from 'react';
import { BrowserRouter } from 'react-router-dom';

import Shell from 'system/style/Shell';
import Home from 'pages/Home';

const Pages = () => {
  return (
    <BrowserRouter>
      <Shell>
        <Home />
      </Shell>
    </BrowserRouter>
  );
};

export default Pages;
