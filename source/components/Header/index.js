import React from 'react';
import PropTypes from 'prop-types';

import './style.scss';

const Header = ({ title, className }) => {
  return (
    <div className={`header ${className}`}>
      {title}
    </div >
  );
};

Header.propTypes = {
  title: PropTypes.string.isRequired,
  className: PropTypes.string
};

Header.defaultProps = {
  className: '',
};

export default Header;
