import React from 'react';
import PropTypes from 'prop-types';

import './style.scss';

const Image = ({ url, className }) => {
  return (
    <img className={`media-image ${className}`} src={url} />
  );
};

Image.propTypes = {
  url: PropTypes.string.isRequired,
  className: PropTypes.string
};

Image.defaultProps = {
  className: '',
};

export default Image;
