import React from 'react';
import PropTypes from 'prop-types';

import Image from './Image';

import './style.scss';

const Media = ({ url, className }) => {
  return (
    <Image className={className} url={url} />
  );
};

Media.propTypes = {
  url: PropTypes.string.isRequired,
  className: PropTypes.string
};

Media.defaultProps = {
  className: '',
};

export default Media;
