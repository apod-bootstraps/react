import React from 'react';
import PropTypes from 'prop-types';

import './style.scss';

const Component = ({ className }) => {
  return (
    <div className={`media ${className}`}>
      component
    </div >
  );
};

Component.propTypes = {
  title: PropTypes.string.isRequired,
  className: PropTypes.string
};

Component.defaultProps = {
  className: '',
};

export default Component;
