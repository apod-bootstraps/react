execute:
	docker run --name react -p 3000:3000 --rm -w /workdir -v .:/workdir node:21-alpine $(command)

install:
	docker run --name ract-install --rm -w /workdir -v .:/workdir node:21-alpine npm install $(package) $(packages)

dev-install:
	docker run --name react-dev-install --rm -w /workdir -v .:/workdir node:21-alpine npm install --save-dev $(package) $(packages)

build:
	docker run --name react-build --rm -w /workdir -v .:/workdir node:21-alpine npm run build

start:
	docker run -it --name react-start -p 3000:3000 --rm -w /workdir -v .:/workdir node:21-alpine npm start
