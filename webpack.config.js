const path = require('path');
const HTMLWebpackPlugin = require('html-webpack-plugin');
const ESLintPlugin = require('eslint-webpack-plugin');

module.exports = {
  mode: 'production',

  plugins: [
    new HTMLWebpackPlugin({
      template: 'public/index.html'
    }),
    new ESLintPlugin()
  ],

  resolve: {
    alias: {
      assets: path.resolve(__dirname, 'source/assets'),
      components: path.resolve(__dirname, 'source/components'),
      library: path.resolve(__dirname, 'source/library'),
      pages: path.resolve(__dirname, 'source/pages'),
      screens: path.resolve(__dirname, 'source/screens'),
      store: path.resolve(__dirname, 'source/store'),
      styles: path.resolve(__dirname, 'source/styles'),
      system: path.resolve(__dirname, 'source/system')
    }
  },

  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {loader: 'babel-loader'},
      },
      {
        test: /\.css$/,
        exclude: /node_modules/,
        use: [
          {loader: 'style-loader'},
          {loader: 'css-loader'}
        ],
      },
      {
        test: /\.scss$/,
        exclude: /node_modules/,
        use: [
          {loader: 'style-loader'},
          {loader: 'css-loader'},
          {loader: 'sass-loader'}
        ],
      },
      {
        test: /\.(png|svg|jpg|gif)$/i,
        exclude: /node_modules/,
        type: 'asset/resource'
      },
    ],
},

  entry: './source/index.js',

  output: {
    clean: true,
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist')
  }
};
